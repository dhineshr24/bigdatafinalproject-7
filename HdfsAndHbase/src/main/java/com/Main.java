package com;

import com.builder.Builder;

public class Main {

    public static void main(String[] args) throws Exception {

        Builder builder = new Builder();
        builder.buildTask(args);

    }
}
