package com.service.csvtohdfs;

import com.config.HadoopConfiguration;
import com.config.VariableSetter;
import com.service.createcsv.BuildingCsvCreator;
import com.service.createcsv.EmployeeCsvCreator;
import com.service.createcsv.PeopleCsvCreator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.util.ArrayList;

public class TaskRunner {
    private String peopleCsvPath;
    private String employeeCsvPath;
    private String resource1;
    private String resource2;
    private String tableName;

    public TaskRunner(String peopleCsvPath, String employeeCsvPath, String resource1,
                      String resource2, String tableName) {
        this.peopleCsvPath = peopleCsvPath;
        this.employeeCsvPath = employeeCsvPath;
        this.resource1 = resource1;
        this.resource2 = resource2;
        this.tableName = tableName;
    }

    public void runTask() {
        try {
            runCsvTask(); //Create CSV files
            runHdfsTask();  //Load all csv files to hdfs
            runHbaseTask(); //Load all csv files to hbase
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runCsvTask() {
        PeopleCsvCreator peopleCsv = new PeopleCsvCreator();
        peopleCsv.create(5, peopleCsvPath, 50);

        EmployeeCsvCreator employeeCsv = new EmployeeCsvCreator();
        employeeCsv.create(1, employeeCsvPath, 100);

        BuildingCsvCreator buildingCsv = new BuildingCsvCreator();
        buildingCsv.create(1, employeeCsvPath, 10);
    }

    public void runHdfsTask() {
        HadoopConfiguration configuration = new HadoopConfiguration(resource1, resource2);
        Configuration conf = configuration.getConfiguration();

        //Load all csv files to hdfs
        String source = VariableSetter.CREATE_PEOPLE_CSV_PATH;
        String destination = VariableSetter.PEOPLE_DESTINATION_FOLDER_HDFS;
        HDFSWriter hdfsWriter = new HDFSWriter();
        hdfsWriter.load(conf, source, destination);

    }

    public void runHbaseTask() throws IOException {
        HadoopConfiguration configuration = new HadoopConfiguration(resource1, resource2);
        Configuration conf = configuration.getConfiguration();

        Path pt = new Path("hdfs://localhost:8020" + VariableSetter.PEOPLE_DESTINATION_FOLDER_HDFS);
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] status = fs.listStatus(pt);
        ArrayList<String> columnFamilyList = new ArrayList<>();
        columnFamilyList.add("personaldata");
        columnFamilyList.add("professionaldata");

        ArrayList<String> columnAttributeList = new ArrayList<>();
        columnAttributeList.add("name");columnAttributeList.add("age");
        columnAttributeList.add("phone_number");columnAttributeList.add("company");
        columnAttributeList.add("building_code");columnAttributeList.add("address");

        HbaseWriter hbaseWriter = new HbaseWriter();
        hbaseWriter.load(status, fs, tableName, columnFamilyList, columnAttributeList);
    }
}


