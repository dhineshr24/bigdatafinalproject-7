package com.service.createcsv;

import au.com.anthonybruno.Gen;
import au.com.anthonybruno.generator.defaults.IntGenerator;
import com.github.javafaker.Faker;
import com.util.ICsvCreator;

public class PeopleCsvCreator implements ICsvCreator {

    @Override
    public void create(int noOfFilesToGenerate, String path, int noOfRowsToGenerate) {
        for (int i = 1; i <= noOfFilesToGenerate; i++) {
            Faker faker = Faker.instance();
            Gen.start()
                    .addField("Name", () -> faker.name().firstName())
                    .addField("age", new IntGenerator(18, 80))
                    .addField("phone_number", () -> faker.phoneNumber().cellPhone())
                    .addField("company", () -> faker.company().name().replaceAll(",", ""))
                    .addField("building_code", () -> faker.address().buildingNumber())
                    .addField("address", () -> faker.address().cityName())
                    .generate(noOfRowsToGenerate)
                    .asCsv()
                    .toFile(path + "people" + i + ".csv");
        }
    }

}
