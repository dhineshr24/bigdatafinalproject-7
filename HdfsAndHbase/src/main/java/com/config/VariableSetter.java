package com.config;

public class VariableSetter {
    public static final String RESOURCE1 = "/opt/homebrew/Cellar/hadoop/3.3" +
            ".0/libexec/etc/hadoop/core-site.xml";
    public static final String RESOURCE2 = "/opt/homebrew/Cellar/hadoop/3.3" +
            ".0/libexec/etc/hadoop/hdfs-site.xml";
    public static final String CREATE_PEOPLE_CSV_PATH = "HdfsAndHbase/src/main/resources" +
            "/PeopleCSVFiles/";
    public static final String CREATE_EMP_BUILDING_CSV_PATH = "HdfsAndHbase/src/main/resources" +
            "/CSVFiles/";
    public static final String PEOPLE_DESTINATION_FOLDER_HDFS = "/CSVFolder";
}

