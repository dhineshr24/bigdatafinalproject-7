package com;

import com.builder.ProtoBuilder;

public class Main {

    public static void main(String[] args) throws Exception {
        ProtoBuilder protoBuilder = new ProtoBuilder();
        protoBuilder.buildTask(args);
    }
}
