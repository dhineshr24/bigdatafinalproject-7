package com.service.loadprototohbase;

import com.util.protoobjects.BuildingOuterClass;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class BuildingHbaseMapper extends Mapper<IntWritable, ImmutableBytesWritable,
        ImmutableBytesWritable, Put> {
    private static final byte[] CF_BYTES1 = Bytes.toBytes("building_details");
    private static final byte[] QUAL_BYTES1 = Bytes.toBytes("building_qual");

    @Override
    protected void map(IntWritable key, ImmutableBytesWritable value, Context context)
            throws IOException, InterruptedException {
        if (key.get() == 0 || value.getLength() == 0) {
            return;
        }

        BuildingOuterClass.Building.Builder building =
                BuildingOuterClass.Building.newBuilder().mergeFrom(value.get());
        byte[] rowKey = Bytes.toBytes(String.valueOf(key));
        Put put = new Put(rowKey);
        //add column in table
        put.addColumn(CF_BYTES1, QUAL_BYTES1, building.build().toByteArray());
        //write context with table row key and  put
        context.write(new ImmutableBytesWritable(rowKey), put);
    }
}


