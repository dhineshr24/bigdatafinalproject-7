package com.service.lowestattendance;

import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.io.IntWritable;

import java.io.IOException;
import java.util.Arrays;

public class AttendanceMapper extends TableMapper<IntWritable, ImmutableBytesWritable> {
    private static final byte[] EMPLOYEETABLE = "employee".getBytes();
    private static final byte[] EmpCF = "employee_details".getBytes();
    private static final byte[] EmpATTR1 = "employee_qual".getBytes();

    @Override
    public void map(ImmutableBytesWritable rowKey, Result columns, Context context) {
        TableSplit currentSplit = (TableSplit) context.getInputSplit();
        byte[] tableName = currentSplit.getTableName(); //get table name from scan
        try {
            if (Arrays.equals(tableName, EMPLOYEETABLE)) {
                ImmutableBytesWritable value = new ImmutableBytesWritable();
                value.set(columns.getValue(EmpCF, EmpATTR1));
                EmployeeOuterClass.Employee.Builder employee =
                        EmployeeOuterClass.Employee.newBuilder().mergeFrom(value.get());

                int building_code = employee.getBuildingCode();
                //write context with building code and employee object
                context.write(new IntWritable(building_code),
                        new ImmutableBytesWritable(employee.build().toByteArray()));
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
