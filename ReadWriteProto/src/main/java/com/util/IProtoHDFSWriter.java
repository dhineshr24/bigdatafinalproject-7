package com.util;

@FunctionalInterface
public interface IProtoHDFSWriter {
    void write();
}
